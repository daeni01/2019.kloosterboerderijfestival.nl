+++
author = "Ineke"
date = "2019-08-13T18:35:38+02:00"
featured_image = "/uploads/foto blog ineke.JPG"
gallery = ["/uploads/foto blog ineke.JPG", "/uploads/P8110548-1.JPG", "/uploads/foto blog ineke 2-1.JPG"]
title = "Proeven aan het kloosterleven, aantrekkingskracht van eenvoud en ritme"

+++
In een klooster leven en werken lijkt mij een roeping voor een minderheid. Maar de aantrekkingskracht van het klooster rijkt ver. Hoe bijzonder om een paar dagen in een klooster te mogen zijn en met anderen te praten over hetzelfde verlangen naar eenvoud en verdieping in je leven.

Hoe vind ik rust? Waarom altijd meer en sneller en beter? Heb ik nog wel tijd voor bezinning, voor God?

Ik had elke dag de keuze om mee te doen aan het ochtendgebed. Tot rust komen, nog voordat ik ontbeten had, deed me echt goed. Ik voelde me fris en vredig aan een nieuwe dag beginnen. Rond lunchtijd opnieuw in stilte en gezang, stilstaan na het werk en gesprekken. Weer een oplaadmoment voor mijn ziel. En het einde van de dag, als alles gedaan is, weer stil worden. Zingen over vrede, bidden voor ontferming voor deze wereld en samen tijd nemen om weer tot rust te komen.

We deden dit niet samen met de monniken (dat mocht wel!) maar in een kleine gemeenschap het ritme van gebed, werken en samen leven (kamperen, koken, spelen, feesten, spelletje doen) ervaren, en dat in de inspirerende omgeving van het klooster en de tuinen. Je hoeft geen monnik te worden om Gods vrede in je dagelijks leven te ervaren. Zoek ernaar, probeer het uit en proef het. Dat smaakt naar meer.