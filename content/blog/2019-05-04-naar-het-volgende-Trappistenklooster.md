---
title: "KloosterBoerderijFestival naar het volgende Trappistenklooster"
date: 2019-05-04T15:16:29+01:00
featured_image: /assets/achtergrond.jpg
author: Wilma
type: blog
---
Als KloosterBoerderijFestival zijn we altijd op zoek naar plekken waar we geïnspireerd kunnen worden en waar we ook tot inspiratie zijn. Naar aanleiding van onze [nominatie voor de religieuzenprijs van de KNR](https://2018.kloosterboerderijfestival.nl/blog/2018-06-08-kloosterboerderijfestival-genomineerd/) werden we uitgenodigd door broeder Bernardus, abt van Abdij Koningshoeven om de mogelijkheden voor een festival daar te onderzoeken.
Het contact met broeder Bernardus was fijn, en er kwamen al snel allemaal vinkjes achter de eisen op ons lijstje. Het enthousiasme van broeder Bernardus over ons, de mogelijkheden die hij ziet, en zijn praktische manier van nadenken zorgt ervoor dat we er zin in krijgen, om hier het festival op te zetten.

En zo gaan we ons festival weer verplaatsen, omvormen, naar de nieuwe plek. Hoe gaat het er uit komen te zien? Als KBF hebben we al verschillende transformaties ondergaan. We begonnen op een boerderij, waar het boerderijaspect goed naar voren kwam, en veel vrijheid was die de festivalsfeer ten goede kwam. De afgelopen 2 jaar waren we op Nieuw Sion, een klooster waar de voetstappen van de monniken nog echoden in de gangen. Het kloosteraspect kwam goed naar voren en ook het pionieren van de groep mensen die daar iets opbouwt zorgde ervoor dat nieuwe ideeën binnen het KBF konden ontkiemen. 

Nu gaan we naar een klooster waar een groep broeders samen leeft, en zijn we als festival te gast bij deze broeders. Het gebedsritme en de gemeenschap die zij leven kunnen inspirerend voor ons zijn. Wij hebben een eigen plek voor ons (Taizé) gebed, maar voor early birds is de mogelijkheid om om kwart over 4 ’s ochtends aan te sluiten bij de metten, en ook op andere momenten kan het gebed van de broeders bezocht worden. 

Zoals onze goede gewoonte tijdens het festival, gaan we ’s ochtends aan het werk. Het werk op Koningshoeven is gevarieerd. Er kunnen mensen samen met broeders aan het werk in de kaasmakerij, de bakkerij, de chocolademakerij en in de tuin, de kassen en het bos bij de abdij. Ook in een naburige geitenboerderij kan waarschijnlijk gewerkt worden. Op die momenten gaan we ook echt gezamenlijk met de broeders aan het werk, en kunnen we in hun leven delen. 

Naast al deze plekken is er nog veel meer bij Koningshoeven: een bierbrouwerij, een imkerij, jammakerij, etc. Misschien is daar ook nog mogelijkheid om daar iets te doen.

Verder is er een grote tuin waar we in kunnen kamperen, genoeg binnenruimte om binnen te eten, workshops te houden, te bidden etc. 

Om kort te zijn, een nieuwe plek die uitnodigend is, waar we kunnen zien hoe een levend klooster is, en ruimte genoeg is voor ons om een week KloosterBoerderijFestival te leven.

We hebben er zin in!

Lees meer over abt Bernardus in een [interview in de Volkskrant van 13 mei](https://www.volkskrant.nl/mensen/god-valt-niet-te-bewijzen-hij-is-er-of-hij-is-er-niet~ba673ba1/)
