+++
author = "KBF"
date = "2019-07-23T05:00:00+00:00"
featured_image = "/uploads/IMG_9247.jpg"
gallery = ["/uploads/IMG_8405.jpg", "/uploads/IMG_8403.jpg", "/uploads/IMG_8179.jpg"]
title = "Niet alleen bidden en werken…"

+++
We zijn hard bezig om naast het werken, het bidden en het ontmoeten ook nog wat andere programma onderdelen te verzorgen. Elke dag heeft een thema. Zo is het thema voor maandag 'rust en ritme in je leven'. Zaterdag is er ruimte voor inhoud en creativiteit. In Iconen komen die bij elkaar. Jim Forest is vredesactivist en schrijver en gaat naar de Russisch Orthodoxe kerkt. Op zaterdag geeft hij een workshop over iconen. Ook is er een workshop iconen schilderen.

Op zaterdag zijn ook andere festivalonderdelen. Wat is een festival zonder **muziek**? Zo geeft Herman een workshop met muziek. En 's avonds is er muziek met Trio Kefi, een mooie mix van balkanfolk en ierse muziek. Met een grote kans op een dansfeestje!

Tijdens het festival willen we de **uitwisseling** van ideeën om idealen vorm te geven stimuleren. Zondag en maandag zullen we daar extra input voor geven door sprekers & workshops te geven. In kleinere groepjes kunnen de deelnemers daarover napraten, samen dromen, zoeken en elkaar inspireren.

Op dinsdag willen we met elkaar nadenken over de huidige **politieke** situatie in Nederland, en vooral ingaan op polarisatie en populisme. Dit doen we door middel van een panel, waarin Ruben Altena interessante sprekers de mogelijkheid geeft om met elkaar in gesprek te gaan. Het doel van het panel is om beter te begrijpen hoe polarisatie, extreem rechts en populisme werken, en het kunnen herkennen. Ook hopen we dat we geïnspireerd raken om manieren te ontdekken hoe hier tegenwicht aan te bieden.

Woensdag is er stilte. Tijd. Rust. En is er ruimte om na te denken over jouw **levensritme**, jouw rust, jouw tijdsindeling.

Op donderdag komt Matty van Leijenhorst die met ons na komt denken over hoe we onze omgeving, en de plaatsen (gebouwen) waar we vaak komen kunnen **verduurzamen**.

Tussen al dat gedenk door is er tijd voor **creativiteit**. Samen of alleen. Koken, land art, streepgedichten, schilderen, verstoppertje spelen en opeens samen een stuk theater improviseren.

**Muur** is de werkvorm die we hebben bedacht om dit allemaal mooi te stroomlijnen. Op de muur kunnen deelnemers ideeën opschrijven. Weet je ergens veel vanaf en wil je daar graag iets over kwijt? Ben je ergens heel erg in geïnteresseerd? Of vraag je je nou altijd al af hoe je veganistisch/glutenvrij/buiten-de-stad/in-een-woongroep/met-meer-stilte-in-je-leven/radicaler-gastvrij kan leven? Via muur vind je zekerweten iemand die je daar iets over kan vertellen!

We hopen dat iedereen met veel mooie dromen, realistische plannen en een gevuld hart weer naar huis terugkeert!