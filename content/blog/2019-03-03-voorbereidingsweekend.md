---
date: 2019-03-03T00:00:00.000Z
title: Voorbereidingsweekend
author: Inge
featured_image: /uploads/IMG_9231.jpg
type: blog
gallery: [IMG_9231.jpg,IMG_9220.jpg,IMG_9221.jpg,IMG_9247.jpg]
resources:
- name: brainstorm
  src: IMG_9231.jpg
  params:
    caption: "Dromen over het KloosterBoerderijFestival van komende zomer"
- name: boom
  src: IMG_9220.jpg
  params:
    caption: "Even tussendoor een boom omzagen"
- name: aziza      
  src: IMG_9221.jpg 
  params:
    caption: "Gastvrijheid in (oud) huize Aziza"
- name: middag
  src: IMG_9247.jpg 
  params:
    caption: "Verdelen van de taken voor de organisatie van het KBF in 2019"



---
Na het [weekend in België](https://2018.kloosterboerderijfestival.nl/blog/2018-12-10-weekend-belgie/) waar er al flink gebrainstormd was waren we op vrijdag 8 februari uitgenodigd in Utrecht bij Gerrald en Hettie thuis. Rond etenstijd waren we van harte welkom. Het rook er dan ook al heerlijk toen ik aankwam. Toen (bijna) iedereen er was gingen we aan tafel en aten heerlijke couscous met groenten.

Na het eten was het tijd voor een kennismakingsrondje met wat je verwacht van het weekend. Daarna een rondje met de ervaringen van de 
bezichtigingen die tot nu toe zijn geweest. [De Buytenhof](http://www.debuytenhof.nl/) in Rhoon is een inspirerende plek, en zeker een aanrader om eens naar toe te gaan. 
Maar als locatie voor het KBF zetten we hem om praktische redenen even op de wachtlijst. De locatie waar we erg enthousiast over zijn (en zij 
over ons!) is abdij [Koningshoeven](https://www.koningshoeven.nl/) in Berkel-Enschot. Nog niet volledig vastgelegd, maar hoogstwaarschijnlijk wel de plek waar we komende zomer 
ons festival gaan houden!

Om 22.00 uur was het tijd voor het gebed in de mooie gebedsruimte waar genoeg plek was voor 9 mensen.
Het bleef nog lang gezellig die avond.

De volgende ochtend begon om 8.00 uur met het gebed. Na het ontbijt begonnen we met de ideeën die waren ingebracht tijdens het weekend in 
België en bedachten daar nog vele plannen bij, deze werden allemaal doorgenomen.

Tussendoor werd er door de heren nog een boom omgezaagd die dreigde om te waaien. Daarna was het alweer tijd voor de lunch en gebed.

In de middag hebben we de dromen van die ochtend onderverdeeld in verschillende onderwerpen met daarbij verschillende taken hierop kon 
iedereen zich inschrijven.

We sloten het weekend af met een heerlijke maaltijd.

Mocht je nou denken het lijkt me leuk om ook mee te denken/ te helpen met organiseren dan kun je [contact opnemen met ons via mail](https://2019.kloosterboerderijfestival.nl/#contact)

Bedankt Gerrald en Hettie voor de gastvrijheid van het mooie weekend!
