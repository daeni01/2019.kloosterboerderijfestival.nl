+++
author = "KBF"
date = "2019-07-23T05:00:00+00:00"
featured_image = "/uploads/IMG_9247.jpg"
gallery = ["/uploads/IMG_8405.jpg", "/uploads/IMG_8403.jpg", "/uploads/IMG_8179.jpg"]
title = "More than praying and working..."

+++
As team, we are busy organising the festival. Not only the (farm) work, the prayers and the time to meet one another, but also the more content related programme. Every day has its own theme. The theme on Wednesday for example is ‘rhythm and rest in your life’. On Saturday, there is a kick-off with content and creativity workshops, which comes together in the theme of icons. Jim Forest, peace activist and writer and member of the Russian Orthodox church, will give a workshop on praying with icons. After this, there will be a workshop painting icons.

On Saturday, also other elements of the festival will get space. What is a festival without music? Herman will give a music workshop. Furthermore, in the evening Trio Kefi will perform their music. Trio Kefi plays music from the Balkan with Irish and Greek influences. There is a big chance the people start to dance :)

An important other element of the festival is the exchange of ideas and ideals. Sunday and Monday we will facilitate this, by speakers and workshops and small groups. This will help the participants of the festival (you!) to share together, to dream together and to find inspire one another.

Tuesday is time to dive into the current political situation in The Netherlands (and in many parts of the world), focusing on polarisation and populism. In a panel situation, Ruben Altena will give space to some interesting people to interact on this theme. The goal is to better understand how polarisation, extreme right and populism function, and how to recognise it. Also we hope to get inspired to find ways how to counter these trends.

After this, a day of silence, time, rest is welcome. And there will be space to think about your own rhythm of life, your rest, the way you spend your time. 

On Thursday, Matty van Leijenhorst will come to think about how we could make our environment and the places (buildings) that we visit often, more sustainable. 

In between all this thinking there is time for creativity. Together or alone. Cooking, land art, line poems, painting, play hide-and-seek or improvise a piece of theatre.

For this we created the method ‘Muur’ (wall) so the we can facilitate all these ideas. Participants can write their ideas on the Muur. So, are you really into a topic, and would you like to share something about it? Are you really interested in a certain issye? Or did you always wonder how to life vegan/glutenfree/out-of-the-city/in-a-community/with-more-silence-in-your-life/practice-radical-hospitality/? Via Muur, you will surely find someone who can tell about it!

We hope that everyone returns home with a lot of beautiful dream, realistic plans and a filled heart!
