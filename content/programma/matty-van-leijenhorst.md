+++
author = "Matty van Leijenhorst"
category = "workshop"
title = "Duurzaamheid in de praktijk"
header = "/uploads/klimaatverandering.jpg"
image = "/uploads/matty.jpg"
when = "2019-08-15T16:00:00+00:00"
link = "https://www.klimaatverbond.nl/klimaatverbond/professionals/matty-van-leijenhorst/"
+++
Matty van Leijenhorst is een jonge vrouw van 29 jaar met passie voor geloof en duurzaamheid. Moeder en echtgenoot in een lief gezin, projectmedewerker bij Klimaatverbond Nederland en vrijwilliger bij A Rocha.

Tijdens de workshop 'duurzaamheid in de praktijk' maak je een plannetje voor duurzame stappen die je graag wilt zetten in je eigen kerk, gemeenschap of buurthuis. En ze zal vertellen over waarom ze als christen duurzaam wil leven, hoe de Bijbel en de natuur haar inspireren en hoe ze om gaat met dilemma's hierin.
