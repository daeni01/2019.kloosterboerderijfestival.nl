---
title: Trio Kefi
vimeo: "288638914"
category: muziek
image: "/uploads/trio-kefi-profiel.png"
header: "/uploads/trio-kefi.png"
when: 2019-08-10T21:00:00+00:00
---
Trio Kefi bestaat uit Pit Hermans, Ramis, en Danielle Roelofsen.
Pit speelt de cimbaal, een liggend snaarinstrument uit de balkan met haar mystieke geluid. Ze speelt folk,  Griekse liederen, balkan muziek en jazz. 
Ramis is percussionist en speelt de ritmes uit de Balkan met flair. 
Danielle speelt wereldmuziek en geïmproviseerde muziek op lier en dwarsfluit, low whistle en Ierse fluit, en ze zingt.

Het trio Kefi repertoire bestaat uit een mengeling van Griekse en Ierse muziek en meer. Wees klaar voor ontspannen sprankelende sound baths en spicy folk songs, er valt zeker wat te dansen!
