+++
author = "Jim Forest"
category = "workshop"
title = "Workshop (bidden met) iconen"
header = "/uploads/jim-forest-kbf-2017.jpg"
image = "/uploads/Jim-Nancy.jpg"
when = "2019-08-10T15:00:00+00:00"
link = "http://jimandnancyforest.com/"
+++
Jim Forest is schrijver van vele boeken, spreker, vredesactivist en Russisch -Orthodox. Tijdens deze workshop vertelt hij over de traditie en betekenis van iconen in de Ooster-Orthodoxe kerk en hoe bidden met iconen een manier kan zijn om je eigen spiritualiteit te verdiepen. Deze lezing zal Engelstalig zijn.
