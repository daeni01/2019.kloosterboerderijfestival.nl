+++
category = "workshop"
when = "2019-08-11T16:00:00+00:00"
image = "uploads/tandenborstel-loesje.png"
title = "Helpt die duurzame tandenborstel?"
vimeo = ""

+++
Helpt die duurzame tandenborstel wel? Ik wil graag duurzaam leven, en veel impact hebben. Wat heeft impact? Afvalscheiden? Niet vliegen? Van bank switchen? Veganistisch eten?

Mieke van Opheusden en Wilma Smilde geven een workshop over wat het nut is van individuele acties, maar ook vooral over wat daarin mist. Hoe kun je je impact vergroten?

Daarna zal Roos Steen een workshop Loesje teksten schrijven geven, waarin we verder nadenken over deze thema's.
